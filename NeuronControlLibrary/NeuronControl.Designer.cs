﻿using System.Windows.Forms;
using System.Drawing;
using System;

namespace NeuronNetworkLibrary
{
    partial class NeuronControl
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblValue = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblValue
            // 
            this.lblValue.AutoSize = true;
            this.lblValue.BackColor = System.Drawing.Color.Transparent;
            this.lblValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblValue.Location = new System.Drawing.Point(11, 17);
            this.lblValue.Name = "lblValue";
            this.lblValue.Size = new System.Drawing.Size(24, 15);
            this.lblValue.TabIndex = 0;
            this.lblValue.Text = "0.0";
            // 
            // NeuronControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.lblValue);
            this.Name = "NeuronControl";
            this.Size = new System.Drawing.Size(50, 50);
            this.VisibleChanged += new System.EventHandler(this.NeuronControl_VisibleChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        #region Implémentation Personnalisée
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Console.WriteLine("OnPaint");
            Graphics g = e.Graphics;

            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            Point location;
            Size size;

            // Drawing Elliptic border

            switch (BorderStyle)
            {
                case BorderStyle.Fixed3D:

                    Size externalSize = new Size(Width - 1, Height - 1);
                    Size internalSize = new Size(Width - 1, Height - 3);

                    Point externalLocation = new Point(0, 0);
                    Point internalLocation = new Point(0, 1);

                    // Dark Side
                    // Dark = top + right

                    Brush darkExternalBrush = new SolidBrush(BorderDarkExternalColor);
                    Brush darkInternalBrush = new SolidBrush(BorderDarkInternalColor);

                    g.FillPie(darkExternalBrush, new Rectangle(externalLocation, externalSize), 135, 180);
                    g.FillPie(darkInternalBrush, new Rectangle(internalLocation, internalSize), 135, 180);

                    // Light Side
                    // Light = bottom + left

                    Brush lightExternalBrush = new SolidBrush(BorderLightExternalColor);
                    Brush lightInternalBrush = new SolidBrush(BorderLightInternalColor);

                    g.FillPie(lightExternalBrush, new Rectangle(externalLocation, externalSize), 315, 180);
                    g.FillPie(lightInternalBrush, new Rectangle(internalLocation, internalSize), 315, 180);

                    break;

                case BorderStyle.FixedSingle:
                    location = new Point(0, 0);
                    size = new Size(Width - 1, Height - 1);

                    Brush borderBrush = new SolidBrush(BorderColor);
                    g.FillEllipse(borderBrush, new Rectangle(location, size));

                    break;

                default:

                    break;
            }

            // Drawing Elliptic background

            switch (BorderStyle)
            {
                case BorderStyle.Fixed3D:

                    location = new Point(2, BorderThickness.Top);
                    size = new Size(Width - 4, Height - 4);

                    break;

                case BorderStyle.FixedSingle:
                    location = new Point(BorderThickness.Left, BorderThickness.Top);
                    size = new Size(Width - BorderThickness.Horizontal - 1, Height - BorderThickness.Vertical - 1);

                    break;

                default:
                    location = new Point(0, 0);
                    size = new Size(Width - 1, Height - 1);

                    break;
            }

            Brush brush = new SolidBrush(BackgroundColor);
            g.FillEllipse(brush, new Rectangle(location, size));
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            Point location = new Point();
            location.X = Width / 2 - lblValue.Width / 2;
            location.Y = Height / 2 - lblValue.Height / 2;
            lblValue.Location = location;
            Refresh();
        }
        #endregion
        private Label lblValue;
    }
}

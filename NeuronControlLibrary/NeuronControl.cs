﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NeuronNetworkLibrary
{
    public enum NeuronType { Normal, Bias }
    public partial class NeuronControl : UserControl
    {
        // These fields back the public properties.

        private bool activatedValue = false;
        private Color activatedBackgroundColorValue = Color.LightGreen;
        private Color borderColorValue = Color.Black;
        private Color borderLightExternalColorValue = Color.White;
        private Color borderLightInternalColorValue = Color.FromArgb(227, 227, 227); // Light Gray
        private Color borderDarkExternalColorValue = Color.FromArgb(160, 160, 160);  // Gray
        private Color borderDarkInternalColorValue = Color.FromArgb(105, 105, 105);  // Dark Gray
        private BorderStyle borderStyleValue = BorderStyle.FixedSingle;
        private Padding borderThicknessValue = new Padding(1);
        private NeuronType typeValue = NeuronType.Normal;
        private Color normalBackgroundColorValue = DefaultBackColor;
        private Color biasBackgroundColorValue = Color.LightBlue;

        public NeuronControl()
        {
            InitializeComponent();
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
        }

        ///////////////////////////////////////////////////////////////////////
        #region Public Properties

        [Category("Neuron")]
        [Browsable(true)]
        public NeuronType Type
        {
            get { return typeValue; }
            set
            {
                if (value != typeValue)
                {
                    typeValue = value;
                    switch (value)
                    {
                        case NeuronType.Normal:
                            BackgroundColor = NormalBackgroundColor;
                            break;
                        case NeuronType.Bias:
                            BackgroundColor = BiasBackgroundColor;
                            break;
                        default:
                            throw new ArgumentOutOfRangeException("Type", "Unknown value.");
                    }
                    Refresh();
                }
            }
        }

        [Category("Neuron")]
        [Browsable(true)]
        public Color BackgroundColor { get; private set; } = DefaultBackColor;

        [Category("Neuron")]
        [Browsable(true)]
        public Color NormalBackgroundColor
        {
            get { return normalBackgroundColorValue; }
            set
            {
                if (value.ToArgb() != normalBackgroundColorValue.ToArgb())
                {
                    normalBackgroundColorValue = value;
                    if (Type == NeuronType.Normal)
                    {
                        BackgroundColor = value;
                        Refresh();
                    }
                }
            }
        }

        [Category("Neuron")]
        [Browsable(true)]
        public Color BiasBackgroundColor
        {
            get { return biasBackgroundColorValue; }
            set
            {
                if (value.ToArgb() != biasBackgroundColorValue.ToArgb())
                {
                    biasBackgroundColorValue = value;
                    if (Type == NeuronType.Bias)
                    {
                        BackgroundColor = value;
                        Refresh();
                    }
                }
            }
        }

        [Category("Neuron")]
        [Browsable(true)]
        public bool Activated
        {
            get { return activatedValue; }
            set
            {
                if (value != activatedValue)
                {
                    activatedValue = value;
                    if (Type == NeuronType.Normal)
                    {
                        if (value) // Activated
                        {
                            BackgroundColor = ActivatedBackgroundColor;
                        }
                        else // Normal
                        {
                            BackgroundColor = NormalBackgroundColor;
                        }
                        Refresh();
                    }
                }
            }
        }

        [Category("Neuron")]
        [Browsable(true)]
        public Color ActivatedBackgroundColor
        {
            get { return activatedBackgroundColorValue; }
            set
            {
                if (value.ToArgb() != activatedBackgroundColorValue.ToArgb())
                {
                    activatedBackgroundColorValue = value;
                    Refresh();
                }
            }
        }

        [Category("Neuron")]
        [Browsable(true)]
        public Color BorderColor
        {
            get { return borderColorValue; }
            set
            {
                if (value.ToArgb() != borderColorValue.ToArgb())
                {
                    borderColorValue = value;
                    Refresh();
                }
            }
        }

        [Category("Neuron")]
        [Browsable(true)]
        public Color BorderLightExternalColor
        {
            get { return borderLightExternalColorValue; }
            set
            {
                if (value.ToArgb() != borderLightExternalColorValue.ToArgb())
                {
                    borderLightExternalColorValue = value;
                    Refresh();
                }
            }
        }

        [Category("Neuron")]
        [Browsable(true)]
        public Color BorderLightInternalColor
        {
            get { return borderLightInternalColorValue; }
            set
            {
                if (value.ToArgb() != borderLightInternalColorValue.ToArgb())
                {
                    borderLightInternalColorValue = value;
                    Refresh();
                }
            }
        }

        [Category("Neuron")]
        [Browsable(true)]
        public Color BorderDarkExternalColor
        {
            get { return borderDarkExternalColorValue; }
            set
            {
                if (value.ToArgb() != borderDarkExternalColorValue.ToArgb())
                {
                    borderDarkExternalColorValue = value;
                    Refresh();
                }
            }
        }

        [Category("Neuron")]
        [Browsable(true)]
        public Color BorderDarkInternalColor
        {
            get { return borderDarkInternalColorValue; }
            set
            {
                if (value.ToArgb() != borderDarkInternalColorValue.ToArgb())
                {
                    borderDarkInternalColorValue = value;
                    Refresh();
                }
            }
        }

        [Category("Neuron")]
        [Browsable(true)]
        public new BorderStyle BorderStyle
        {
            get { return borderStyleValue; }
            set
            {
                borderStyleValue = value;
                Refresh();
            }
        }

        [Category("Neuron")]
        [Browsable(true)]
        public Padding BorderThickness
        {
            get { return borderThicknessValue; }
            set
            {
                borderThicknessValue = value;
                Refresh();
            }
        }

        [Browsable(false)]
        public List<NeuronLinkLabel> InputLinks { get; set; } = new List<NeuronLinkLabel>();

        [Browsable(false)]
        public List<NeuronLinkLabel> OutputLinks { get; set; } = new List<NeuronLinkLabel>();

        [Browsable(false)]
        public NeuronLayerControl Layer { get; set; }
        #endregion

        private void NeuronControl_VisibleChanged(object sender, EventArgs e)
        {
            foreach(NeuronLinkLabel link in InputLinks)
            {
                link.Visible = Visible;
            }
            foreach (NeuronLinkLabel link in OutputLinks)
            {
                link.Visible = Visible;
            }
        }
    }
}

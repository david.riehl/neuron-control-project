﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NeuronNetworkLibrary
{
    public partial class NeuronLinkLabel : Label
    {
        private decimal value;

        [Category("NeuronLinkLabel")]
        [Browsable(true)]
        public decimal Value
        {
            get { return value; }
            set
            {
                if (value != this.value)
                {
                    this.value = value;
                    Text = value.ToString();
                }
            }
        }

        [Category("NeuronLinkLabel")]
        [Browsable(true)]
        public new string Text
        {
            get { return base.Text; }
            set
            {
                if (decimal.TryParse(value, out decimal number))
                {
                    base.Text = Math.Round(number, 1).ToString();
                    Value = number;
                }
                else
                {
                    //throw new ArgumentException("value must be decimal");
                }
            }
        }

        public NeuronControl InputNeuron { get; set; }
        public NeuronControl OutputNeuron { get; set; }

        public NeuronLinkLabel() : base()
        {
            InitializeComponent();
            Text = "0,0";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace NeuronNetworkLibrary
{
    public partial class NeuronNetworkControl : UserControl
    {
        // These fields back the public properties.

        private bool showBiasNeuronValue = true;
        private bool hideOutputBiasNeuronValue = true;

        public NeuronNetworkControl()
        {
            InitializeComponent();
            MyInitializeComponent();
        }

        ///////////////////////////////////////////////////////////////////////
        #region Public Properties

        [Category("Neuron Network")]
        [Browsable(true)]
        public bool ShowBiasNeuron
        {
            get { return showBiasNeuronValue; }
            set
            {
                if (value != showBiasNeuronValue)
                {
                    showBiasNeuronValue = value;
                    NeuronLayerControl outputLayer = Layers[Layers.Count - 1] as NeuronLayerControl;
                    foreach (NeuronLayerControl neuronLayer in Layers)
                    {
                        if (neuronLayer == outputLayer)
                        {
                            if (!HideOutputBiasNeuron)
                            {
                                outputLayer.Neurons[0].Visible = value;
                            }
                        }
                        else
                        {
                            neuronLayer.Neurons[0].Visible = value;
                        }
                    }
                    Refresh();
                }
            }
        }

        [Category("Neuron Network")]
        [Browsable(true)]
        public bool HideOutputBiasNeuron
        {
            get { return hideOutputBiasNeuronValue; }
            set
            {
                if (value != hideOutputBiasNeuronValue)
                {
                    hideOutputBiasNeuronValue = value;
                    NeuronLayerControl outputLayer = Layers[Layers.Count - 1] as NeuronLayerControl;
                    if (ShowBiasNeuron)
                    {
                        outputLayer.Neurons[0].Visible = !value;
                    }
                    Refresh();
                }
            }
        }

        [Category("Neuron Network")]
        [Browsable(true)]
        public string Title
        {
            get { return grpNetwork.Text; }
            set
            {
                if (value != grpNetwork.Text)
                {
                    grpNetwork.Text = value;
                }
            }
        }

        [Category("Neuron Network")]
        [Browsable(true)]
        public int LayersCount
        {
            get { return (int)numLayers.Value; }
            set
            {
                if (value != numLayers.Value)
                {
                    numLayers.Value = value;
                }
            }
        }

        [Browsable(false)]
        public List<NeuronLayerControl> Layers { get; private set; } = new List<NeuronLayerControl>();

        #endregion

        private void pnlLayers_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            // AntiAliasing for a better display
            g.SmoothingMode = SmoothingMode.AntiAlias;

            Pen pen = new Pen(Color.Black, 1.0F);
            pen.DashStyle = DashStyle.Dash;

            int nbLayers = Convert.ToInt32(numLayers.Value);

            for (int layerIndex = 0; layerIndex < nbLayers - 1; layerIndex++)
            {
                NeuronLayerControl nextLayer = Layers[layerIndex + 1];
                NeuronLayerControl prevLayer = Layers[layerIndex];

                foreach (NeuronControl nc2 in nextLayer.Neurons)
                {
                    if (nc2.Type != NeuronType.Bias)
                    {
                        int x2 = nextLayer.Location.X + nextLayer.GroupBox.Location.X + nc2.Location.X + nc2.Width / 2;
                        int y2 = nextLayer.Location.Y + nextLayer.GroupBox.Location.Y + nc2.Location.Y + nc2.Height / 2;

                        Point pt2 = new Point(x2, y2);

                        foreach (NeuronLinkLabel link in nc2.InputLinks)
                        {
                            NeuronControl nc1 = link.InputNeuron as NeuronControl;
                            if (nc1.Visible)
                            {
                                int x1 = prevLayer.Location.X + prevLayer.GroupBox.Location.X + nc1.Location.X + nc1.Width / 2;
                                int y1 = prevLayer.Location.Y + prevLayer.GroupBox.Location.Y + nc1.Location.Y + nc1.Height / 2;

                                Point pt1 = new Point(x1, y1);

                                // Draw Link
                                g.DrawLine(pen, pt1, pt2);

                                int x = x1 + (x2 - x1) / 2 - link.Width / 2;
                                int y = y1 + (y2 - y1) / 2 - link.Height / 2;
                                Point location = new Point(x, y);


                                link.Location = location;
                                pnlLayers.PerformLayout(link, "Location");
                            }
                        }
                    }
                }
            }
        }

        private void numLayers_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NeuronNetworkLibrary
{
    public partial class NeuronLayerControl : UserControl
    {
        public NeuronLayerControl()
        {
            InitializeComponent();
            MyInitializeComponent();
        }

        ///////////////////////////////////////////////////////////////////////
        #region Public Properties

        [Category("Neuron Layer")]
        [Browsable(true)]
        public bool ShowBiasNeuron
        {
            get { return biasNeuronControl.Visible; }
            set
            {
                if (value != biasNeuronControl.Visible)
                {
                    biasNeuronControl.Visible = value;
                }
            }
        }

        [Category("Neuron Layer")]
        [Browsable(true)]
        public string Title
        {
            get { return grpLayer.Text; }
            set
            {
                if (value != grpLayer.Text)
                {
                    grpLayer.Text = value;
                }
            }
        }
        
        [Category("Neuron Layer")]
        [Browsable(true)]
        public ControlCollection Neurons
        {
            get { return grpLayer.Controls; }
        }

        [Category("Neuron Layer")]
        [Browsable(true)]
        public GroupBox GroupBox
        {
            get { return grpLayer; }
        }

        [Browsable(false)]
        public NeuronNetworkControl Network { get; set; }
        #endregion

        private void numLayers_ValueChanged(object sender, EventArgs e)
        {
            int count = Neurons.Count;
            int value = Convert.ToInt32(numLayers.Value);

            if (value < count - 1)
            {
                // Remove some Neurons
                for (int index = count - 1; index > value; index--)
                {
                    NeuronControl neuron = Neurons[index] as NeuronControl;
                    // cleaning input links
                    foreach (NeuronLinkLabel link in neuron.InputLinks)
                    {
                        link.InputNeuron.OutputLinks.Remove(link);
                        link.Parent.Controls.Remove(link);
                    }
                    // cleaning output links
                    foreach (NeuronLinkLabel link in neuron.OutputLinks)
                    {
                        link.OutputNeuron.InputLinks.Remove(link);
                        link.Parent.Controls.Remove(link);
                    }
                    Neurons.Remove(neuron);
                }
            }
            else if (value > count - 1)
            {
                // Add some Neurons
                NeuronControl previous = (NeuronControl)Neurons[count - 1];
                NeuronLayerControl prevLayer;
                NeuronLayerControl nextLayer;

                int layerIndex = Network.Layers.FindIndex(l => l == this);


                for (int index = count - 1; index < value; index++)
                {
                    int prevX = previous.Location.X;
                    int prevY = previous.Location.Y;

                    NeuronControl neuron = new NeuronControl();
                    neuron.Location = new Point(prevX, prevY + 56);
                    Neurons.Add(neuron);

                    if (layerIndex > 0)
                    {
                        prevLayer = Network.Layers[layerIndex - 1];
                        // add links with previous layer
                        foreach (NeuronControl nc in prevLayer.Neurons)
                        {
                            NeuronLinkLabel link = new NeuronLinkLabel();
                            neuron.InputLinks.Add(link);
                            link.OutputNeuron = neuron;

                            nc.OutputLinks.Add(link);
                            link.InputNeuron = nc;

                            Parent.Controls.Add(link);
                            link.BringToFront();
                        }
                    }

                    if (layerIndex < Network.Layers.Count - 1)
                    {
                        nextLayer = Network.Layers[layerIndex + 1];
                        // add links with next layer
                        foreach (NeuronControl nc in nextLayer.Neurons)
                        {
                            if (nc.Type != NeuronType.Bias)
                            {
                                NeuronLinkLabel link = new NeuronLinkLabel();
                                neuron.OutputLinks.Add(link);
                                link.InputNeuron = neuron;

                                nc.InputLinks.Add(link);
                                link.OutputNeuron = nc;

                                Parent.Controls.Add(link);
                                link.Parent = Parent;
                                link.BringToFront();
                            }
                        }
                    }
                }
            }
            Parent.Refresh();
        }
    }
}

﻿namespace NeuronNetworkLibrary
{
    partial class NeuronLayerControl
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpLayer = new System.Windows.Forms.GroupBox();
            this.neuronControl1 = new NeuronNetworkLibrary.NeuronControl();
            this.biasNeuronControl = new NeuronNetworkLibrary.NeuronControl();
            this.numLayers = new System.Windows.Forms.NumericUpDown();
            this.lblNeurons = new System.Windows.Forms.Label();
            this.grpLayer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numLayers)).BeginInit();
            this.SuspendLayout();
            // 
            // grpLayer
            // 
            this.grpLayer.AutoSize = true;
            this.grpLayer.BackColor = System.Drawing.Color.Transparent;
            this.grpLayer.Controls.Add(this.biasNeuronControl);
            this.grpLayer.Controls.Add(this.neuronControl1);
            this.grpLayer.Location = new System.Drawing.Point(0, 39);
            this.grpLayer.Margin = new System.Windows.Forms.Padding(0);
            this.grpLayer.Name = "grpLayer";
            this.grpLayer.Size = new System.Drawing.Size(62, 144);
            this.grpLayer.TabIndex = 4;
            this.grpLayer.TabStop = false;
            this.grpLayer.Text = "Layer (1)";
            // 
            // neuronControl1
            // 
            this.neuronControl1.Activated = false;
            this.neuronControl1.ActivatedBackgroundColor = System.Drawing.Color.LightGreen;
            this.neuronControl1.BackColor = System.Drawing.Color.Transparent;
            this.neuronControl1.BiasBackgroundColor = System.Drawing.Color.LightBlue;
            this.neuronControl1.BorderColor = System.Drawing.Color.Black;
            this.neuronControl1.BorderDarkExternalColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(160)))), ((int)(((byte)(160)))));
            this.neuronControl1.BorderDarkInternalColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.neuronControl1.BorderLightExternalColor = System.Drawing.Color.White;
            this.neuronControl1.BorderLightInternalColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(227)))), ((int)(((byte)(227)))));
            this.neuronControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.neuronControl1.BorderThickness = new System.Windows.Forms.Padding(1);
            this.neuronControl1.Location = new System.Drawing.Point(6, 75);
            this.neuronControl1.Name = "neuronControl1";
            this.neuronControl1.NormalBackgroundColor = System.Drawing.SystemColors.Control;
            this.neuronControl1.Size = new System.Drawing.Size(50, 50);
            this.neuronControl1.TabIndex = 5;
            this.neuronControl1.Type = NeuronNetworkLibrary.NeuronType.Normal;
            // 
            // biasNeuronControl
            // 
            this.biasNeuronControl.Activated = false;
            this.biasNeuronControl.ActivatedBackgroundColor = System.Drawing.Color.LightGreen;
            this.biasNeuronControl.BackColor = System.Drawing.Color.Transparent;
            this.biasNeuronControl.BiasBackgroundColor = System.Drawing.Color.LightBlue;
            this.biasNeuronControl.BorderColor = System.Drawing.Color.Black;
            this.biasNeuronControl.BorderDarkExternalColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(160)))), ((int)(((byte)(160)))));
            this.biasNeuronControl.BorderDarkInternalColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.biasNeuronControl.BorderLightExternalColor = System.Drawing.Color.White;
            this.biasNeuronControl.BorderLightInternalColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(227)))), ((int)(((byte)(227)))));
            this.biasNeuronControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.biasNeuronControl.BorderThickness = new System.Windows.Forms.Padding(1);
            this.biasNeuronControl.Location = new System.Drawing.Point(6, 19);
            this.biasNeuronControl.Name = "biasNeuronControl";
            this.biasNeuronControl.NormalBackgroundColor = System.Drawing.SystemColors.Control;
            this.biasNeuronControl.Size = new System.Drawing.Size(50, 50);
            this.biasNeuronControl.TabIndex = 4;
            this.biasNeuronControl.Type = NeuronNetworkLibrary.NeuronType.Bias;
            // 
            // numLayers
            // 
            this.numLayers.Location = new System.Drawing.Point(6, 16);
            this.numLayers.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numLayers.Name = "numLayers";
            this.numLayers.Size = new System.Drawing.Size(50, 20);
            this.numLayers.TabIndex = 3;
            this.numLayers.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numLayers.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numLayers.ValueChanged += new System.EventHandler(this.numLayers_ValueChanged);
            // 
            // lblNeurons
            // 
            this.lblNeurons.AutoSize = true;
            this.lblNeurons.Location = new System.Drawing.Point(3, 0);
            this.lblNeurons.Name = "lblNeurons";
            this.lblNeurons.Size = new System.Drawing.Size(53, 13);
            this.lblNeurons.TabIndex = 2;
            this.lblNeurons.Text = "Neurons :";
            // 
            // NeuronLayerControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.grpLayer);
            this.Controls.Add(this.numLayers);
            this.Controls.Add(this.lblNeurons);
            this.Name = "NeuronLayerControl";
            this.Size = new System.Drawing.Size(62, 183);
            this.grpLayer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numLayers)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode personnalisée pour la prise en charge du concepteur
        /// </summary>
        private void MyInitializeComponent()
        {
            biasNeuronControl.Layer = this;
            neuronControl1.Layer = this;
        }
        #endregion
        private System.Windows.Forms.GroupBox grpLayer;
        private System.Windows.Forms.NumericUpDown numLayers;
        private System.Windows.Forms.Label lblNeurons;
        private NeuronControl biasNeuronControl;
        private NeuronControl neuronControl1;
    }
}

﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace NeuronNetworkLibrary
{
    partial class NeuronNetworkControl
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblLayers = new System.Windows.Forms.Label();
            this.numLayers = new System.Windows.Forms.NumericUpDown();
            this.grpConfirguration = new System.Windows.Forms.GroupBox();
            this.pnlLayers = new System.Windows.Forms.Panel();
            this.neuronLinkLabel2 = new NeuronNetworkLibrary.NeuronLinkLabel();
            this.neuronLinkLabel1 = new NeuronNetworkLibrary.NeuronLinkLabel();
            this.inputNeuronLayerControl = new NeuronNetworkLibrary.NeuronLayerControl();
            this.outputNeuronLayerControl = new NeuronNetworkLibrary.NeuronLayerControl();
            this.grpNetwork = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.numLayers)).BeginInit();
            this.grpConfirguration.SuspendLayout();
            this.pnlLayers.SuspendLayout();
            this.grpNetwork.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblLayers
            // 
            this.lblLayers.AutoSize = true;
            this.lblLayers.Location = new System.Drawing.Point(12, 21);
            this.lblLayers.Name = "lblLayers";
            this.lblLayers.Size = new System.Drawing.Size(44, 13);
            this.lblLayers.TabIndex = 0;
            this.lblLayers.Text = "Layers :";
            // 
            // numLayers
            // 
            this.numLayers.Location = new System.Drawing.Point(62, 19);
            this.numLayers.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numLayers.Name = "numLayers";
            this.numLayers.Size = new System.Drawing.Size(40, 20);
            this.numLayers.TabIndex = 1;
            this.numLayers.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numLayers.ValueChanged += new System.EventHandler(this.numLayers_ValueChanged);
            // 
            // grpConfirguration
            // 
            this.grpConfirguration.Controls.Add(this.numLayers);
            this.grpConfirguration.Controls.Add(this.lblLayers);
            this.grpConfirguration.Location = new System.Drawing.Point(3, 3);
            this.grpConfirguration.Name = "grpConfirguration";
            this.grpConfirguration.Size = new System.Drawing.Size(198, 131);
            this.grpConfirguration.TabIndex = 2;
            this.grpConfirguration.TabStop = false;
            this.grpConfirguration.Text = "Neuron Network Configuration";
            // 
            // pnlLayers
            // 
            this.pnlLayers.AutoScroll = true;
            this.pnlLayers.AutoSize = true;
            this.pnlLayers.Controls.Add(this.neuronLinkLabel2);
            this.pnlLayers.Controls.Add(this.neuronLinkLabel1);
            this.pnlLayers.Controls.Add(this.inputNeuronLayerControl);
            this.pnlLayers.Controls.Add(this.outputNeuronLayerControl);
            this.pnlLayers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlLayers.Location = new System.Drawing.Point(3, 16);
            this.pnlLayers.Margin = new System.Windows.Forms.Padding(0);
            this.pnlLayers.Name = "pnlLayers";
            this.pnlLayers.Size = new System.Drawing.Size(253, 266);
            this.pnlLayers.TabIndex = 6;
            this.pnlLayers.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlLayers_Paint);
            // 
            // neuronLinkLabel2
            // 
            this.neuronLinkLabel2.AutoSize = true;
            this.neuronLinkLabel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.neuronLinkLabel2.InputNeuron = null;
            this.neuronLinkLabel2.Location = new System.Drawing.Point(77, 138);
            this.neuronLinkLabel2.Name = "neuronLinkLabel2";
            this.neuronLinkLabel2.OutputNeuron = null;
            this.neuronLinkLabel2.Size = new System.Drawing.Size(24, 15);
            this.neuronLinkLabel2.TabIndex = 3;
            this.neuronLinkLabel2.Text = "0,0";
            this.neuronLinkLabel2.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // neuronLinkLabel1
            // 
            this.neuronLinkLabel1.AutoSize = true;
            this.neuronLinkLabel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.neuronLinkLabel1.InputNeuron = null;
            this.neuronLinkLabel1.Location = new System.Drawing.Point(77, 82);
            this.neuronLinkLabel1.Name = "neuronLinkLabel1";
            this.neuronLinkLabel1.OutputNeuron = null;
            this.neuronLinkLabel1.Size = new System.Drawing.Size(24, 15);
            this.neuronLinkLabel1.TabIndex = 2;
            this.neuronLinkLabel1.Text = "0,0";
            this.neuronLinkLabel1.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // inputNeuronLayerControl
            // 
            this.inputNeuronLayerControl.AutoSize = true;
            this.inputNeuronLayerControl.BackColor = System.Drawing.Color.Transparent;
            this.inputNeuronLayerControl.Location = new System.Drawing.Point(6, 6);
            this.inputNeuronLayerControl.Margin = new System.Windows.Forms.Padding(6);
            this.inputNeuronLayerControl.Name = "inputNeuronLayerControl";
            this.inputNeuronLayerControl.Network = null;
            this.inputNeuronLayerControl.ShowBiasNeuron = true;
            this.inputNeuronLayerControl.Size = new System.Drawing.Size(62, 183);
            this.inputNeuronLayerControl.TabIndex = 0;
            this.inputNeuronLayerControl.Title = "Input";
            // 
            // outputNeuronLayerControl
            // 
            this.outputNeuronLayerControl.AutoSize = true;
            this.outputNeuronLayerControl.BackColor = System.Drawing.Color.Transparent;
            this.outputNeuronLayerControl.Location = new System.Drawing.Point(110, 6);
            this.outputNeuronLayerControl.Margin = new System.Windows.Forms.Padding(6);
            this.outputNeuronLayerControl.Name = "outputNeuronLayerControl";
            this.outputNeuronLayerControl.Network = null;
            this.outputNeuronLayerControl.ShowBiasNeuron = false;
            this.outputNeuronLayerControl.Size = new System.Drawing.Size(62, 183);
            this.outputNeuronLayerControl.TabIndex = 1;
            this.outputNeuronLayerControl.Title = "Output";
            // 
            // grpNetwork
            // 
            this.grpNetwork.Controls.Add(this.pnlLayers);
            this.grpNetwork.Location = new System.Drawing.Point(207, 3);
            this.grpNetwork.Name = "grpNetwork";
            this.grpNetwork.Size = new System.Drawing.Size(259, 285);
            this.grpNetwork.TabIndex = 7;
            this.grpNetwork.TabStop = false;
            this.grpNetwork.Text = "Network";
            // 
            // NeuronNetworkControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grpNetwork);
            this.Controls.Add(this.grpConfirguration);
            this.Name = "NeuronNetworkControl";
            this.Size = new System.Drawing.Size(469, 291);
            ((System.ComponentModel.ISupportInitialize)(this.numLayers)).EndInit();
            this.grpConfirguration.ResumeLayout(false);
            this.grpConfirguration.PerformLayout();
            this.pnlLayers.ResumeLayout(false);
            this.pnlLayers.PerformLayout();
            this.grpNetwork.ResumeLayout(false);
            this.grpNetwork.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        #region Code Personnalisé pour le Concepteur de composants
        /// <summary> 
        /// Méthode personnalisée pour la prise en charge du concepteur
        /// </summary>
        private void MyInitializeComponent()
        {
            NeuronControl inputLayerBiasNeuron = inputNeuronLayerControl.Neurons[0] as NeuronControl;
            NeuronControl inputLayerNeuron = inputNeuronLayerControl.Neurons[1] as NeuronControl;
            NeuronControl outputLayerNeuron = outputNeuronLayerControl.Neurons[1] as NeuronControl;

            // Configure Neuron Links
            neuronLinkLabel1.InputNeuron = inputLayerBiasNeuron;
            neuronLinkLabel1.OutputNeuron = outputLayerNeuron;

            neuronLinkLabel2.InputNeuron = inputLayerNeuron;
            neuronLinkLabel2.OutputNeuron = outputLayerNeuron;

            // Add Input and Output Neuron Links
            inputLayerBiasNeuron.OutputLinks.Add(neuronLinkLabel1);
            inputLayerNeuron.OutputLinks.Add(neuronLinkLabel2);

            outputLayerNeuron.InputLinks.Add(neuronLinkLabel1);
            outputLayerNeuron.InputLinks.Add(neuronLinkLabel2);

            // Add Input and Output Layers
            Layers.Add(inputNeuronLayerControl);
            Layers.Add(outputNeuronLayerControl);

            // Link to network
            inputNeuronLayerControl.Network = this;
            outputNeuronLayerControl.Network = this;
        }
        #endregion

        private System.Windows.Forms.Label lblLayers;
        private System.Windows.Forms.NumericUpDown numLayers;
        private System.Windows.Forms.GroupBox grpConfirguration;
        private Panel pnlLayers;
        private GroupBox grpNetwork;
        private NeuronLayerControl outputNeuronLayerControl;
        private NeuronLayerControl inputNeuronLayerControl;
        private NeuronLinkLabel neuronLinkLabel2;
        private NeuronLinkLabel neuronLinkLabel1;
    }
}

﻿namespace NeuronNetworkTest
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.neuronNetworkControl1 = new NeuronNetworkLibrary.NeuronNetworkControl();
            this.SuspendLayout();
            // 
            // neuronNetworkControl1
            // 
            this.neuronNetworkControl1.HideOutputBiasNeuron = true;
            this.neuronNetworkControl1.LayersCount = 2;
            this.neuronNetworkControl1.Location = new System.Drawing.Point(12, 12);
            this.neuronNetworkControl1.Name = "neuronNetworkControl1";
            this.neuronNetworkControl1.ShowBiasNeuron = true;
            this.neuronNetworkControl1.Size = new System.Drawing.Size(469, 291);
            this.neuronNetworkControl1.TabIndex = 0;
            this.neuronNetworkControl1.Title = "Network";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.neuronNetworkControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private NeuronNetworkLibrary.NeuronNetworkControl neuronNetworkControl1;
    }
}

